# Hatsuna

Improve farming in Minecraft.

**Programmer Art is strongly recommended for this mod.** No support will be given for the new Minecraft textures.

## Features

### Data-driven growth system

Hatsuna supports setting several properties of plant growth:

* the growth time. For vanilla plants, this is scaled so that `1.0` corresponds to the vanilla growth rate. A higher value results in slower growth.
* a list of favored and disfavored biomes, which speed up or slow down crop growth. Additionally, some plants have a chance of dying out in disfavored biomes:
  * The growth stage of crops growing on farmland has a chance of regressing in disfavored biomes (killing the crop if already on the earliest stage). Mature crops do not regress in growth.
  * Sweet berry bushes, bamboo, cacti, and sugar cane are not subjected to dying out.

The following plants respect the growth system:

* Farmland crops:
    * wheat
    * carrots
    * potatoes
    * beetroots
    * pumpkins
    * melons
    * torchflowers
    * pitcher pods
* Overworld trees:
    * 2×2 trees take four times longer to grow than their small counterparts.
* Plants using `AbstractPlantPartBlock`:
    * cave vines
    * twisting vines
    * weeping vines
    * kelp
* Sweet berries
* Bamboo
* Cacti
* Cocoa
* Sugar cane

The following naturally growing plants *do not yet* respect the growth system:

* chorus fruit
* regular vines
* mushrooms (spread)
* nether wart

### Farmland fertilization

Using bone meal on a crop that grows on farmland no longer advances its growth stage immediately; instead, it adds fertilization to the farmland under it, causing crops to grow faster on it. The chance of increasing fertilization per bonemeal decreases as the existing fertilization level increases. Crops also deplete fertilization from the underlying farmland.

Other soil-type blocks have fixed fertilization levels, although some of these blocks can transform into others when depleted.

### Stew durability

Stews have been moved to use a durability-based system. The suspicious stew recipe, however, has not yet been tweaked to compensate.

### Planned features

* [X] Crops have a chance of dying when in disfavored biomes
* [X] Replace bonemealing crops with fertilization of farmland, and nerf bone meal in other situations
  * Maybe: add fertilization levels for dirt and grass, and make items made of organic matter have a chance to increase fertilization when despawning
* [ ] Nerf animal breeding and growth (breeding only has a chance to induce breeding mode)
* [ ] Add pests (perhaps only in harder difficulties)?
* [ ] Add new crop types (some of which are found only as chest loot)
* [ ] Give desert villages more appropriate crop choices
