package xyz.flirora.hatsuna.core;

import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.world.biome.Biome;
import xyz.flirora.hatsuna.HatsunaMod;

public class HatsunaTags {
    public static final TagKey<Biome> EMPTY = TagKey.of(RegistryKeys.BIOME, HatsunaMod.id("empty"));
}
