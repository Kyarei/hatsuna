package xyz.flirora.hatsuna.core;

import net.minecraft.world.Difficulty;

public enum BiomeAppraisal {
    PREFERRED,
    NEUTRAL,
    DISPREFERRED;

    public double getCropDecayChance(Difficulty difficulty) {
        if (this != DISPREFERRED) return 0.0;
        return switch (difficulty) {
            case PEACEFUL -> 0.0075;
            case EASY -> 0.0075;
            case NORMAL -> 0.015;
            case HARD -> 0.025;
        };
    }
}
