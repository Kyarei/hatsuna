package xyz.flirora.hatsuna.core;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.entry.RegistryEntry;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.world.biome.Biome;

public record BiomePreferences(TagKey<Biome> preferredBiomes, TagKey<Biome> dispreferredBiomes) {
    public static final Codec<BiomePreferences> CODEC = RecordCodecBuilder.create(inst -> inst.group(
                    TagKey.codec(RegistryKeys.BIOME).optionalFieldOf("preferred_biomes", HatsunaTags.EMPTY).forGetter(BiomePreferences::preferredBiomes),
                    TagKey.codec(RegistryKeys.BIOME).optionalFieldOf("dispreferred_biomes", HatsunaTags.EMPTY).forGetter(BiomePreferences::dispreferredBiomes))
            .apply(inst, BiomePreferences::new));
    public static final BiomePreferences INDIFFERENT = new BiomePreferences(HatsunaTags.EMPTY, HatsunaTags.EMPTY);

    public BiomeAppraisal appraise(RegistryEntry<Biome> biome) {
        if (biome.isIn(this.preferredBiomes)) return BiomeAppraisal.PREFERRED;
        if (biome.isIn(this.dispreferredBiomes)) return BiomeAppraisal.DISPREFERRED;
        return BiomeAppraisal.NEUTRAL;
    }
}
