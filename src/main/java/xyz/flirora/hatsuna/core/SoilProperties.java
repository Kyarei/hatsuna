package xyz.flirora.hatsuna.core;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.registry.Registries;

import java.util.Map;
import java.util.Optional;

public record SoilProperties(int fertility, double depletionChance, Optional<BlockState> depletionResult) {
    public static final Codec<SoilProperties> CODEC = RecordCodecBuilder.create(inst -> inst.group(
                    Codec.INT.optionalFieldOf("fertility", 0).forGetter(SoilProperties::fertility),
                    Codec.DOUBLE.optionalFieldOf("depletion_chance", 0.01).forGetter(SoilProperties::depletionChance),
                    BlockState.CODEC.optionalFieldOf("depletion_result").forGetter(SoilProperties::depletionResult))
            .apply(inst, SoilProperties::new));
    public static final Codec<Map<Block, SoilProperties>> MULTI_CODEC = Codec.simpleMap(Registries.BLOCK.getCodec(), SoilProperties.CODEC, Registries.BLOCK).codec();
    public static final SoilProperties DEFAULT = new SoilProperties(0, 0.01, Optional.empty());
}
