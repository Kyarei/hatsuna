package xyz.flirora.hatsuna.core;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.mojang.serialization.JsonOps;
import net.fabricmc.fabric.api.resource.SimpleSynchronousResourceReloadListener;
import net.minecraft.block.Block;
import net.minecraft.resource.Resource;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;
import xyz.flirora.hatsuna.HatsunaMod;
import xyz.flirora.hatsuna.crops.Croperties;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GrowableProperties implements SimpleSynchronousResourceReloadListener {
    private static final Gson GSON = new Gson();
    private static final GrowableProperties INSTANCE = new GrowableProperties();
    private final Map<Block, Croperties> croperties = new HashMap<>();
    private final Map<Block, SoilProperties> soilProperties = new HashMap<>();

    public static GrowableProperties getInstance() {
        return INSTANCE;
    }

    public void reload(ResourceManager manager) {
        croperties.clear();
        for (Resource resource : manager.getAllResources(HatsunaMod.id("croperties.json"))) {
            try (BufferedReader input = resource.getReader()) {
                JsonObject json = GSON.fromJson(input, JsonObject.class);
                var cropertyList = Croperties.MULTI_CODEC.decode(JsonOps.INSTANCE, json).getOrThrow(false, e -> {
                    throw new JsonParseException(e);
                }).getFirst();
                croperties.putAll(cropertyList);
            } catch (IOException e) {
                HatsunaMod.LOGGER.error("Failed to read croperties.", e);
            }
        }
        soilProperties.clear();
        for (Resource resource : manager.getAllResources(HatsunaMod.id("soil_properties.json"))) {
            try (BufferedReader input = resource.getReader()) {
                JsonObject json = GSON.fromJson(input, JsonObject.class);
                var soilPropertyList = SoilProperties.MULTI_CODEC.decode(JsonOps.INSTANCE, json).getOrThrow(false, e -> {
                    throw new JsonParseException(e);
                }).getFirst();
                soilProperties.putAll(soilPropertyList);
            } catch (IOException e) {
                HatsunaMod.LOGGER.error("Failed to read soil properties.", e);
            }
        }
    }

    public Croperties getCroperties(Block block) {
        return croperties.getOrDefault(block, Croperties.DEFAULT);
    }

    @Nullable
    public Croperties getCropertiesOrNull(Block block) {
        return croperties.get(block);
    }

    public SoilProperties getSoilProperties(Block block) {
        return soilProperties.getOrDefault(block, SoilProperties.DEFAULT);
    }

    @Nullable
    public SoilProperties getSoilPropertiesOrNull(Block block) {
        return soilProperties.get(block);
    }

    @Override
    public Identifier getFabricId() {
        return HatsunaMod.id("growable_properties");
    }
}
