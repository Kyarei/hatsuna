package xyz.flirora.hatsuna.core;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.state.property.IntProperty;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import xyz.flirora.hatsuna.crops.Croperties;

import java.util.function.Supplier;

public class TemporalProcesses {
    public static boolean processPlantDecay(BlockState blockState, World world, BlockPos pos, Supplier<BlockState> replacement) {
        Croperties croperties = GrowableProperties.getInstance().getCroperties(blockState.getBlock());
        double decayChance = croperties.biomePreferences().appraise(world.getBiome(pos)).getCropDecayChance(world.getDifficulty());
        if (world.getRandom().nextDouble() < decayChance) {
            world.setBlockState(pos, replacement.get(), Block.NOTIFY_ALL | Block.SKIP_DROPS);
            return true;
        }
        return false;
    }

    public static boolean processStagedPlantDecay(BlockState blockState, World world, BlockPos pos, int age, IntProperty ageProperty, Random random) {
        return processPlantDecay(blockState, world, pos, () -> {
            int lowerBound = Math.min(age / 2 + 1, age - 1);
            return age == 0 ?
                    Blocks.AIR.getDefaultState() :
                    blockState.with(
                            ageProperty,
                            random.nextBetween(lowerBound, age - 1));
        });
    }

    public static void processSoilDepletion(World world, BlockPos pos, Random random) {
        BlockPos groundPos = pos.down();
        BlockState ground = world.getBlockState(groundPos);
        var fertilization = ground.getOrEmpty(HatsunaProperties.FERTILIZATION);
        if (fertilization.isPresent()) {
            if (fertilization.get() > 0 && random.nextInt(100) == 0) {
                world.setBlockState(groundPos, ground.with(HatsunaProperties.FERTILIZATION, fertilization.get() - 1));
            }
        } else {
            SoilProperties properties = GrowableProperties.getInstance().getSoilProperties(ground.getBlock());
            if (properties.depletionResult().isPresent() && random.nextDouble() < properties.depletionChance()) {
                world.setBlockState(groundPos, properties.depletionResult().get());
            }
        }
    }
}
