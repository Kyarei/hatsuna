package xyz.flirora.hatsuna.core;

import net.minecraft.state.property.IntProperty;

public class HatsunaProperties {
    public static final IntProperty FERTILIZATION = IntProperty.of("hatsuna_fertilization", 0, Fertilization.MAX_VALUE);
}
