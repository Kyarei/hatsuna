package xyz.flirora.hatsuna.core;

import net.minecraft.block.BlockState;
import net.minecraft.block.FarmlandBlock;
import net.minecraft.block.Fertilizable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldView;

public class Fertilization {
    public static final int MAX_VALUE = 15;

    public static int ofBlock(BlockState state) {
        return state.getOrEmpty(HatsunaProperties.FERTILIZATION).orElseGet(() -> GrowableProperties.getInstance().getSoilProperties(state.getBlock()).fertility());
    }

    public static void withGround(WorldView world, BlockPos pos, GroundProcessor callback) {
        BlockPos groundPos = pos.down();
        BlockState ground = world.getBlockState(groundPos);
        if (ground.getBlock() instanceof FarmlandBlock farmland) {
            Fertilizable fertilizable = (Fertilizable) farmland;
            callback.accept(fertilizable, groundPos, ground);
        }
    }

    @FunctionalInterface
    public interface GroundProcessor {
        void accept(Fertilizable fertilizable, BlockPos groundPos, BlockState ground);
    }
}
