package xyz.flirora.hatsuna.mixin;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.StewItem;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(StewItem.class)
public class StewItemMixin {
    @Inject(method = "finishUsing", at = @At("RETURN"), cancellable = true)
    private void onFinishUsing(ItemStack stack, World world, LivingEntity user, CallbackInfoReturnable<ItemStack> cir) {
        if (user instanceof PlayerEntity player && player.getAbilities().creativeMode) {
            return;
        }
        // Oh no you don't
        stack.increment(1);
        stack.damage(1, user, entity -> {});
        if (stack.getCount() == 0) {
            cir.setReturnValue(new ItemStack(Items.BOWL));
        } else {
            cir.setReturnValue(stack);
        }
    }
}
