package xyz.flirora.hatsuna.mixin.floratweaks;

import net.minecraft.block.BambooBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Fertilizable;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import xyz.flirora.hatsuna.core.Fnz;
import xyz.flirora.hatsuna.core.GrowableProperties;

@Mixin(BambooBlock.class)
public abstract class BambooBlockMixin extends Block implements Fertilizable {
    public BambooBlockMixin(Settings settings) {
        super(settings);
    }

    @Redirect(method = "randomTick", at = @At(value = "INVOKE", target = "Lnet/minecraft/util/math/random/Random;nextInt(I)I"))
    private int redirectGrowthRoll(Random random, int defo, BlockState state, ServerWorld world, BlockPos pos, Random random2) {
        float growthTime = GrowableProperties.getInstance().getCroperties(this).getEffectiveGrowthTime(world, pos);
        return Fnz.nextInt(random, defo * growthTime);
    }
}
