package xyz.flirora.hatsuna.mixin.floratweaks;

import net.minecraft.block.*;
import net.minecraft.block.sapling.LargeTreeSaplingGenerator;
import net.minecraft.block.sapling.SaplingGenerator;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import xyz.flirora.hatsuna.core.Fnz;
import xyz.flirora.hatsuna.core.GrowableProperties;
import xyz.flirora.hatsuna.core.TemporalProcesses;

@Mixin(SaplingBlock.class)
public abstract class SaplingBlockMixin extends PlantBlock implements Fertilizable {
    @Shadow
    @Final
    private SaplingGenerator generator;

    public SaplingBlockMixin(Settings settings) {
        super(settings);
    }

    @Inject(method = "randomTick", at = @At("HEAD"), cancellable = true)
    private void onRandomTick(BlockState state, ServerWorld world, BlockPos pos, Random random, CallbackInfo ci) {
        if (TemporalProcesses.processPlantDecay(state, world, pos, Blocks.DEAD_BUSH::getDefaultState)) ci.cancel();
    }

    @Redirect(method = "randomTick", at = @At(value = "INVOKE", target = "Lnet/minecraft/util/math/random/Random;nextInt(I)I"))
    private int redirectGrowthRoll(Random random, int defo, BlockState state, ServerWorld world, BlockPos pos, Random random2) {
        float growthTime = GrowableProperties.getInstance().getCroperties(this).getEffectiveGrowthTime(world, pos);
        if (this.generator instanceof LargeTreeSaplingGenerator && LargeTreeSaplingGenerator.canGenerateLargeTree(state, world, pos, 0, 0)) {
            growthTime *= 4;
        }
        return Fnz.nextInt(random, defo * growthTime);
    }

    @ModifyConstant(method = "canGrow", constant = @Constant(doubleValue = 0.45))
    private double modifySaplingFertilizationChance(double defo, World world, Random random, BlockPos pos, BlockState state) {
        float growthTime = GrowableProperties.getInstance().getCroperties(this).getEffectiveGrowthTime(world, pos);
        return 0.45 * 7 / growthTime;
    }
}
