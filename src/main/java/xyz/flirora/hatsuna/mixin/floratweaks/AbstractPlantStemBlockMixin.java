package xyz.flirora.hatsuna.mixin.floratweaks;

import net.minecraft.block.*;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.random.Random;
import net.minecraft.util.shape.VoxelShape;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import xyz.flirora.hatsuna.core.GrowableProperties;
import xyz.flirora.hatsuna.core.TemporalProcesses;

@Mixin(AbstractPlantStemBlock.class)
public abstract class AbstractPlantStemBlockMixin extends AbstractPlantPartBlock implements Fertilizable {
    @Shadow
    @Final
    private double growthChance;

    protected AbstractPlantStemBlockMixin(Settings settings, Direction growthDirection, VoxelShape outlineShape, boolean tickWater) {
        super(settings, growthDirection, outlineShape, tickWater);
    }

    @Inject(method = "randomTick", at = @At("HEAD"), cancellable = true)
    private void onRandomTick(BlockState state, ServerWorld world, BlockPos pos, Random random, CallbackInfo ci) {
        if (TemporalProcesses.processPlantDecay(state, world, pos, Blocks.AIR::getDefaultState)) ci.cancel();
    }

    @Redirect(method = "randomTick", at = @At(value = "FIELD", opcode = Opcodes.GETFIELD, target = "Lnet/minecraft/block/AbstractPlantStemBlock;growthChance:D"))
    private double modifyGrowthChance(AbstractPlantStemBlock instance, BlockState state, ServerWorld world, BlockPos pos, Random random) {
        return this.growthChance / GrowableProperties.getInstance().getCroperties(this).getEffectiveGrowthTime(world, pos);
    }
}
