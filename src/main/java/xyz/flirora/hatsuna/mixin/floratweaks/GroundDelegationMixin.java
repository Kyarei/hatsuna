package xyz.flirora.hatsuna.mixin.floratweaks;

import net.minecraft.block.*;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import net.minecraft.world.WorldView;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import xyz.flirora.hatsuna.core.Fertilization;

/**
 * Common mixin for all blocks that grow on farmland in order to override their fertilization methods to delegate to the block they are on.
 */
@Mixin(value = {CropBlock.class, PitcherCropBlock.class, StemBlock.class})
public abstract class GroundDelegationMixin implements Fertilizable {
    @Inject(method = "isFertilizable", at = @At("HEAD"), cancellable = true)
    private void overrideIsFertilizable(WorldView world, BlockPos pos, BlockState state, boolean isClient, CallbackInfoReturnable<Boolean> cir) {
        Fertilization.withGround(world, pos, (f, groundPos, ground) -> cir.setReturnValue(f.isFertilizable(world, groundPos, ground, isClient)));
    }

    @Inject(method = "canGrow(Lnet/minecraft/world/World;Lnet/minecraft/util/math/random/Random;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;)Z", at = @At("HEAD"), cancellable = true)
    private void overrideCanGrow(World world, Random random, BlockPos pos, BlockState state, CallbackInfoReturnable<Boolean> cir) {
        Fertilization.withGround(world, pos, (f, groundPos, ground) -> cir.setReturnValue(f.canGrow(world, random, groundPos, ground)));
    }

    @Inject(method = "grow", at = @At("HEAD"), cancellable = true)
    private void overrideGrow(ServerWorld world, Random random, BlockPos pos, BlockState state, CallbackInfo ci) {
        Fertilization.withGround(world, pos, (f, groundPos, ground) -> {
            f.grow(world, random, groundPos, ground);
            ci.cancel();
        });
    }
}
