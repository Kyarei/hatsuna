package xyz.flirora.hatsuna.mixin.floratweaks;

import net.minecraft.block.BlockState;
import net.minecraft.block.CropBlock;
import net.minecraft.block.Fertilizable;
import net.minecraft.block.PlantBlock;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.IntProperty;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import xyz.flirora.hatsuna.core.GrowableProperties;
import xyz.flirora.hatsuna.core.TemporalProcesses;

@Mixin(value = CropBlock.class)
public abstract class CropBlockMixin extends PlantBlock implements Fertilizable {
    @Shadow
    @Final
    public static IntProperty AGE;

    @Shadow
    public abstract int getMaxAge();

    @Shadow
    public abstract int getAge(BlockState state);

    @Shadow
    protected abstract IntProperty getAgeProperty();

    public CropBlockMixin(Settings settings) {
        super(settings);
    }

    // Unfortunately, this only has a 1/3 chance of firing for BeetrootsBlock and TorchflowerBlock.
    @Inject(method = "randomTick", at = @At("HEAD"), cancellable = true)
    private void onRandomTick(BlockState state, ServerWorld world, BlockPos pos, Random random, CallbackInfo ci) {
        int age = getAge(state);
        if (age < getMaxAge()) {
            if (TemporalProcesses.processStagedPlantDecay(state, world, pos, age, getAgeProperty(), random)) {
                ci.cancel();
            }
            TemporalProcesses.processSoilDepletion(world, pos, random);
        }
    }

    @ModifyConstant(method = "randomTick", constant = @Constant(floatValue = 25.0f))
    private float getGrowthTime(float defo, BlockState state, ServerWorld world, BlockPos pos, Random random) {
        return defo * GrowableProperties.getInstance().getCroperties(this).getEffectiveGrowthTime(world, pos);
    }
}
