package xyz.flirora.hatsuna.mixin.floratweaks;

import net.minecraft.block.*;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.IntProperty;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import xyz.flirora.hatsuna.core.GrowableProperties;
import xyz.flirora.hatsuna.core.TemporalProcesses;

@Mixin(StemBlock.class)
public abstract class StemBlockMixin extends PlantBlock implements Fertilizable {
    @Shadow
    @Final
    public static IntProperty AGE;

    public StemBlockMixin(Settings settings) {
        super(settings);
    }

    @Inject(method = "randomTick", at = @At("HEAD"), cancellable = true)
    private void onRandomTick(BlockState state, ServerWorld world, BlockPos pos, Random random, CallbackInfo ci) {
        int age = state.get(AGE);
        if (age < 7) {
            if (TemporalProcesses.processPlantDecay(state, world, pos, Blocks.AIR::getDefaultState)) {
                ci.cancel();
            }
            TemporalProcesses.processSoilDepletion(world, pos, random);
        }
    }

    @ModifyConstant(method = "randomTick", constant = @Constant(floatValue = 25.0f))
    private float getGrowthTime(float defo, BlockState state, ServerWorld world, BlockPos pos, Random random) {
        return defo * GrowableProperties.getInstance().getCroperties(this).getEffectiveGrowthTime(world, pos);
    }
}
