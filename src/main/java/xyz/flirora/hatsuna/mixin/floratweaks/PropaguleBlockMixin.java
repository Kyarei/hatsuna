package xyz.flirora.hatsuna.mixin.floratweaks;

import net.minecraft.block.*;
import net.minecraft.block.sapling.SaplingGenerator;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.IntProperty;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import xyz.flirora.hatsuna.core.Fnz;
import xyz.flirora.hatsuna.core.GrowableProperties;
import xyz.flirora.hatsuna.core.TemporalProcesses;

@Mixin(PropaguleBlock.class)
public abstract class PropaguleBlockMixin extends SaplingBlock implements Waterloggable {

    @Shadow
    @Final
    public static IntProperty AGE;

    public PropaguleBlockMixin(SaplingGenerator generator, Settings settings) {
        super(generator, settings);
    }

    @Inject(method = "randomTick", at = @At("HEAD"), cancellable = true)
    private void onRandomTick(BlockState state, ServerWorld world, BlockPos pos, Random random, CallbackInfo ci) {
        if (TemporalProcesses.processPlantDecay(state, world, pos, Blocks.DEAD_BUSH::getDefaultState))
            ci.cancel();
    }

    @Redirect(method = "randomTick", at = @At(value = "INVOKE", target = "Lnet/minecraft/util/math/random/Random;nextInt(I)I"))
    private int redirectGrowthRoll(Random random, int defo, BlockState state, ServerWorld world, BlockPos pos, Random random2) {
        float growthTime = GrowableProperties.getInstance().getCroperties(this).getEffectiveGrowthTime(world, pos);
        return Fnz.nextInt(random, defo * growthTime);
    }

    // No need to mix into canGrow; it delegates to SaplingBlock's version.
}
