package xyz.flirora.hatsuna.mixin.floratweaks;

import net.minecraft.block.BlockState;
import net.minecraft.block.Fertilizable;
import net.minecraft.block.PitcherCropBlock;
import net.minecraft.block.TallPlantBlock;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.IntProperty;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import xyz.flirora.hatsuna.core.GrowableProperties;
import xyz.flirora.hatsuna.core.TemporalProcesses;

@Mixin(PitcherCropBlock.class)
public abstract class PitcherCropBlockMixin extends TallPlantBlock implements Fertilizable {
    @Shadow
    @Final
    public static IntProperty AGE;

    public PitcherCropBlockMixin(Settings settings) {
        super(settings);
    }

    @Inject(method = "randomTick", at = @At("HEAD"), cancellable = true)
    private void onRandomTick(BlockState state, ServerWorld world, BlockPos pos, Random random, CallbackInfo ci) {
        int age = state.get(AGE);
        if (age < 4) {
            if (TemporalProcesses.processStagedPlantDecay(state, world, pos, age, AGE, random)) {
                ci.cancel();
            }
            TemporalProcesses.processSoilDepletion(world, pos, random);
        }
    }

    @ModifyConstant(method = "randomTick", constant = @Constant(floatValue = 25.0f))
    private float getGrowthTime(float defo, BlockState state, ServerWorld world, BlockPos pos, Random random) {
        return defo * GrowableProperties.getInstance().getCroperties(this).getEffectiveGrowthTime(world, pos);
    }
}
