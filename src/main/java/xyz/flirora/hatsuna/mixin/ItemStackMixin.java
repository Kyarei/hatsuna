package xyz.flirora.hatsuna.mixin;

import net.minecraft.block.Block;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import xyz.flirora.hatsuna.core.GrowableProperties;
import xyz.flirora.hatsuna.crops.Croperties;

import java.util.List;

@Mixin(ItemStack.class)
public abstract class ItemStackMixin {
    @Shadow
    public abstract Item getItem();

    @Inject(method = "getTooltip", at = @At("RETURN"))
    private void appendCropTooltipData(@Nullable PlayerEntity player, TooltipContext context, CallbackInfoReturnable<List<Text>> cir) {
        List<Text> list = cir.getReturnValue();
        if (context.isAdvanced() && player != null && this.getItem() instanceof BlockItem blockItem) {
            Block block = blockItem.getBlock();
            Croperties croperties = GrowableProperties.getInstance().getCropertiesOrNull(block);
            if (croperties != null) {
                switch (croperties.biomePreferences().appraise(player.getWorld().getBiome(player.getBlockPos()))) {
                    case PREFERRED -> {
                        list.add(Text.translatable("hatsuna.crop.preferred").formatted(Formatting.GREEN));
                    }
                    case NEUTRAL -> {
                    }
                    case DISPREFERRED -> {
                        list.add(Text.translatable("hatsuna.crop.dispreferred").formatted(Formatting.RED));
                    }
                }
            }
        }
    }
}
