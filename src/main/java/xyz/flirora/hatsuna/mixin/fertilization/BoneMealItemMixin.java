package xyz.flirora.hatsuna.mixin.fertilization;

import net.minecraft.item.BoneMealItem;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(BoneMealItem.class)
public class BoneMealItemMixin {
    // ...
}
