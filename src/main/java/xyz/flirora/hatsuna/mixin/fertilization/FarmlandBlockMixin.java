package xyz.flirora.hatsuna.mixin.fertilization;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FarmlandBlock;
import net.minecraft.block.Fertilizable;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.StateManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import net.minecraft.world.WorldView;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import xyz.flirora.hatsuna.core.Fertilization;
import xyz.flirora.hatsuna.core.HatsunaProperties;

@Mixin(FarmlandBlock.class)
public abstract class FarmlandBlockMixin extends Block implements Fertilizable {
    public FarmlandBlockMixin(Settings settings) {
        super(settings);
    }

    @Inject(method = "appendProperties", at = @At("RETURN"))
    private void addCustomProperties(StateManager.Builder<Block, BlockState> builder, CallbackInfo ci) {
        builder.add(HatsunaProperties.FERTILIZATION);
    }

    @ModifyArg(method = "<init>", at = @At(value = "INVOKE", target = "Lnet/minecraft/block/FarmlandBlock;setDefaultState(Lnet/minecraft/block/BlockState;)V"))
    private BlockState initCustomProperties(BlockState state) {
        return state.with(HatsunaProperties.FERTILIZATION, 0);
    }

    public boolean isFertilizable(WorldView world, BlockPos pos, BlockState state, boolean isClient) {
        return state.get(HatsunaProperties.FERTILIZATION) < Fertilization.MAX_VALUE;
    }

    public boolean canGrow(World world, Random random, BlockPos pos, BlockState state) {
        int fertilization = state.get(HatsunaProperties.FERTILIZATION);
        double successChance = 1.0 / (1.0 + 0.1 * fertilization * fertilization);
        return random.nextDouble() < successChance;
    }

    public void grow(ServerWorld world, Random random, BlockPos pos, BlockState state) {
        world.setBlockState(pos, state.with(HatsunaProperties.FERTILIZATION, Math.min(Fertilization.MAX_VALUE, state.get(HatsunaProperties.FERTILIZATION) + 1)));
    }
}
