package xyz.flirora.hatsuna.mixin.fertilization;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.HoeItem;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import xyz.flirora.hatsuna.core.Fertilization;
import xyz.flirora.hatsuna.core.HatsunaProperties;

import java.util.function.Consumer;

@Mixin(HoeItem.class)
public class HoeItemMixin {
    @Unique
    private static void farmlandTillAction(ItemUsageContext context) {
        World world = context.getWorld();
        BlockPos pos = context.getBlockPos();
        BlockState curBlock = world.getBlockState(pos);
        int fertility = curBlock.getOrEmpty(HatsunaProperties.FERTILIZATION).orElse(0);
        if (fertility < 0) fertility = 0;
        if (fertility > Fertilization.MAX_VALUE) fertility = Fertilization.MAX_VALUE;
        BlockState result = Blocks.FARMLAND.getDefaultState().with(HatsunaProperties.FERTILIZATION, fertility);
        world.setBlockState(
                pos, result,
                Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
        world.emitGameEvent(
                GameEvent.BLOCK_CHANGE, pos,
                GameEvent.Emitter.of(context.getPlayer(), result));
    }

    @Inject(method = "createTillAction", at = @At("HEAD"), cancellable = true)
    private static void createCustomTillActionForFarmlandResult(BlockState result, CallbackInfoReturnable<Consumer<ItemUsageContext>> cir) {
        if (result.isOf(Blocks.FARMLAND)) {
            cir.setReturnValue(HoeItemMixin::farmlandTillAction);
        }
    }
}
