package xyz.flirora.hatsuna.mixin;

import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.Slice;

@Mixin(Items.class)
public class ItemsMixin {
    @ModifyArg(method = "<clinit>", at = @At(value = "INVOKE", target = "Lnet/minecraft/item/StewItem;<init>(Lnet/minecraft/item/Item$Settings;)V", ordinal = 0), slice = @Slice(from = @At(value = "CONSTANT", args = "stringValue=mushroom_stew")), index = 0)
    private static Item.Settings mushroomStewSettings(Item.Settings defo) {
        return new Item.Settings().maxCount(1).maxDamage(4).food(new FoodComponent.Builder().hunger(3).saturationModifier(0.6F).build());
    }

    @ModifyArg(method = "<clinit>", at = @At(value = "INVOKE", target = "Lnet/minecraft/item/StewItem;<init>(Lnet/minecraft/item/Item$Settings;)V", ordinal = 0), slice = @Slice(from = @At(value = "CONSTANT", args = "stringValue=rabbit_stew")), index = 0)
    private static Item.Settings rabbitStew(Item.Settings defo) {
        return new Item.Settings().maxCount(1).maxDamage(5).food(new FoodComponent.Builder().hunger(3).saturationModifier(0.6F).build());
    }

    @ModifyArg(method = "<clinit>", at = @At(value = "INVOKE", target = "Lnet/minecraft/item/StewItem;<init>(Lnet/minecraft/item/Item$Settings;)V", ordinal = 0), slice = @Slice(from = @At(value = "CONSTANT", args = "stringValue=beetroot_soup")), index = 0)
    private static Item.Settings beetrootSoup(Item.Settings defo) {
        return new Item.Settings().maxCount(1).maxDamage(3).food(new FoodComponent.Builder().hunger(3).saturationModifier(0.6F).build());
    }

    @ModifyArg(method = "<clinit>", at = @At(value = "INVOKE", target = "Lnet/minecraft/item/SuspiciousStewItem;<init>(Lnet/minecraft/item/Item$Settings;)V", ordinal = 0), slice = @Slice(from = @At(value = "CONSTANT", args = "stringValue=suspicious_stew")), index = 0)
    private static Item.Settings suspiciousStew(Item.Settings defo) {
        return new Item.Settings().maxCount(1).maxDamage(3).food(new FoodComponent.Builder().hunger(3).saturationModifier(0.6F).alwaysEdible().build());
    }
}
