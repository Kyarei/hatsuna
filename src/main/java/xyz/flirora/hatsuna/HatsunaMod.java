package xyz.flirora.hatsuna;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xyz.flirora.hatsuna.core.GrowableProperties;

public class HatsunaMod implements ModInitializer {
    public static final String MOD_ID = "hatsuna";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

    public static Identifier id(String key) {
        return new Identifier(MOD_ID, key);
    }

    @Override
    public void onInitialize() {
        LOGGER.info("Initializing Hatsuna.");

        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(GrowableProperties.getInstance());
    }
}