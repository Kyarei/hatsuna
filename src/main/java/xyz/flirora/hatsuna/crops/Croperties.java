package xyz.flirora.hatsuna.crops;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.block.Block;
import net.minecraft.registry.Registries;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import xyz.flirora.hatsuna.core.BiomePreferences;
import xyz.flirora.hatsuna.core.Fertilization;

import java.util.Map;

public record Croperties(float growthTime, BiomePreferences biomePreferences) {
    public static final Codec<Croperties> CODEC = RecordCodecBuilder.create(inst -> inst.group(
                    Codec.FLOAT.fieldOf("growth_time").forGetter(Croperties::growthTime),
                    BiomePreferences.CODEC.optionalFieldOf("biome_preferences", BiomePreferences.INDIFFERENT).forGetter(Croperties::biomePreferences))
            .apply(inst, Croperties::new));
    public static final Codec<Map<Block, Croperties>> MULTI_CODEC = Codec.simpleMap(Registries.BLOCK.getCodec(), Croperties.CODEC, Registries.BLOCK).codec();
    public static final Croperties DEFAULT = new Croperties(1.0f, BiomePreferences.INDIFFERENT);

    public float getEffectiveGrowthTime(World world, BlockPos pos) {
        float biomeFactor = switch (biomePreferences.appraise(world.getBiome(pos))) {
            case PREFERRED -> 0.8f;
            case NEUTRAL -> 1.0f;
            case DISPREFERRED -> 1.5f;
        };
        int fertilization = Fertilization.ofBlock(world.getBlockState(pos));
        // TODO: refine formula
        float fertilizationFactor = 1.0f + 0.1f * fertilization;
        return this.growthTime * biomeFactor * fertilizationFactor;
    }
}
